<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<% request.setCharacterEncoding("utf-8"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>




<button type="submit" id="btn_t1">ElectedPersonInfoServlet</button>
<button type="submit" id="btn_t2">ElectedPersonPromise</button>
<button type="submit" id="btn_t3">KnowledgeServlet</button>
</br>
<button type="submit" id="btn_t4">CitizenPosterInsertServlet</button>
<button type="submit" id="btn_t5">CitizenMainGetListServlet</button>
<button type="submit" id="btn_t6">CitizenGetListServlet</button>
</br>
<button type="submit" id="btn_t7">CitizenGetCommentListServlet</button>
<button type="submit" id="btn_t8">CitizenCommentInsertServlet</button>

<script src="http://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
<script>

	$(function(){
		$("#btn_t1").click(function(){
			
			$.ajax({
                url: 'ElectedPersonInfoServlet',
                method: 'GET',
                data : "ep_id=1",
                dataType : 'JSON',
                contentType: "application/json; charset=UTF-8",
                success: function(res) {
                	if(res.code=="SUCCESS"){
                		console.log(res.person)
    					console.log(res.profile)
    					console.log(res.promise_num)
                		
                	}else{
                		console.log("unknown error");
                	}
                    },
                error:function(){
                    console.log("unknown error");
                }
            });
		})
		
		$("#btn_t2").click(function(){
			
			$.ajax({
                url: 'ElectedPersonPromiseServlet',
                method: 'GET',
                data : "ep_id=1"+"&category=city",
                contentType: "application/json; charset=UTF-8",
                dataType : 'JSON',
                success: function(res) {
                	if(res.code=="SUCCESS"){
                		console.log(res.promise)
    					console.log(res.count)                		
                	}else{
                		console.log("unknown error");
                	}
                    },
                error:function(){
                    console.log("unknown error");
                }
            });
			
		})
		$("#btn_t3").click(function(){
				
				$.ajax({
	                url: 'KnowledgeServlet',
	                method: 'GET',
	                data : "offset=5&limit=5",
	                contentType: "application/json; charset=UTF-8",
	                dataType : 'JSON',
	                success: function(res) {
	                	if(res.code=="SUCCESS"){
	                		console.log(res.list)
      
	                	}else{
	                		console.log("unknown error");
	                	}
	                    },
	                error:function(){ 
	                    console.log("unknown error");
	                }
	            });	
		})
		
		$("#btn_t4").click(function(){

				$.ajax({
	                url: 'CitizenPosterInsertServlet',
	                method: 'GET',
	                data : "u_id=1&title=testInsert&category=복지&comments=잘될까요?????????안될까요??????&gu=1",
	                contentType: "application/json; charset=UTF-8",
	                dataType : 'JSON',
	                success: function(res) {
	                	if(res.code=="SUCCESS"){
	                		console.log(res.id)
      
	                	}else{
	                		console.log("unknown error");
	                	}
	                    },
	                error:function(){
	                    console.log("unknown error");
	                }
	            });	
		})
		
		$("#btn_t5").click(function(){

				$.ajax({
	                url: 'CitizenMainGetListServlet',
	                method: 'GET',
	                contentType: "application/json; charset=UTF-8",
	                dataType : 'JSON',
	                success: function(res) {
	                	if(res.code=="SUCCESS"){
	                		console.log(res.help)
	                		console.log(res.say)
	                		console.log(res.post)
      
	                	}else{
	                		console.log("unknown error");
	                	}
	                    },
	                error:function(){
	                    console.log("unknown error");
	                }
	            });	
		})
		
		$("#btn_t6").click(function(){

				$.ajax({
	                url: 'CitizenGetListServlet',
	                method: 'GET',
	                contentType: "application/json; charset=UTF-8",
	                data:"offset=0&category=help",
	                dataType : 'JSON',
	                success: function(res) {
	                	if(res.code=="SUCCESS"){
	                		console.log(res.list)	               
      
	                	}else{
	                		console.log("unknown error");
	                	}
	                    },
	                error:function(){
	                    console.log("unknown error");
	                }
	            });	
		})
		
		$("#btn_t7").click(function(){

				$.ajax({
	                url: 'CitizenGetCommentListServlet',
	                method: 'GET',
	                contentType: "application/json; charset=UTF-8",
	                data:"offset=10&category=help&id=1",
	                dataType : 'JSON',
	                success: function(res) {
	                	if(res.code=="SUCCESS"){
	                		console.log(res.list)	               
      
	                	}else{
	                		console.log("unknown error");
	                	}
	                    },
	                error:function(){
	                    console.log("unknown error");
	                }
	            });	
		})
		
		$("#btn_t8").click(function(){
				$.ajax({
	                url: 'GoodOrBadUpdateServlet',
	                method: 'GET',
	                contentType: "application/json; charset=UTF-8",
	                data:"category=help&c_id=10&u_id=3&option=update",
	                dataType : 'JSON',
	                success: function(res) {
	                	if(res.code=="SUCCESS"){
	                		console.log(res.id)	               
      
	                	}else{
	                		console.log("unknown error");
	                	}
	                    },
	                error:function(){
	                    console.log("unknown error");
	                }
	            });	
		})
	})
</script>
</body>
</html>