package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;

public class ElectedProfileDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
	public JSONArray getElectedProfile(int ep_id) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT category,contents,date FROM elected_profile WHERE ep_id = ? order by category";
    	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, ep_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"category","contents","date"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
//	public static void main(String[] args){
//		ElectedProfileDAO dao = new ElectedProfileDAO();
//		JSONArray ja = dao.getElectedProfile(1);
//		try{
//		for(int i =0 ; i< ja.length();i++){
//			JSONObject obj = ja.getJSONObject(i);
//			System.out.println(obj.toString());
//		}
//		}catch (Exception e) {
//			// TODO: handle exception
//		}
//	}

}
