package model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONObject;

public class CitizenPosterDAO {

	private Connection conn;
    private PreparedStatement pstmt;
    
    //Create
    public int registerCitizenPoster(int u_id, String title, String category, String comments, String gu,String priture) {
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO citizen_poster( u_id, title, category, comments, good, hits, create_date, gu,priture)" +
    			"VALUES (?,?,?,?,0,0,now(),?,?)";
    	try {
    		pstmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
    		
    		//pstmt.setString(1, id);
    		pstmt.setInt(1,u_id);
    		pstmt.setString(2, title);
    		pstmt.setString(3, category);
    		pstmt.setString(4, comments);
    		pstmt.setString(5, gu);
    		pstmt.setString(6, priture);
    		pstmt.executeUpdate();
    		
    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);      		
    		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }

    
    //Update
    public boolean updateCitizenPoster(String id, String title, String comments) {
        conn = DBManager.getConnection();
        String sql = "UPDATE citizen_poster SET title=?, comments = ? where id =?";

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, title);
            pstmt.setString(2, comments);
            pstmt.setString(3, id);
            pstmt.executeUpdate();
            DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
    }
    
    
    //delete
    public boolean deleteCitizenPoster(String ID) {
        conn = DBManager.getConnection();
        String sql = "DELETE FROM citizen_poster WHERE id = ?";

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, ID);
            pstmt.executeUpdate();
            DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
    }

    

    public JSONObject getCitizenPost(int id) {
		JSONObject jres = new JSONObject();
    	
		conn = DBManager.getConnection();
		String sql = "SELECT id,u_id,title,category,comments,good,hits,create_date,priture, gu FROM citizen_poster WHERE id = ?";
    	
		try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","title","category","comments","good","hits","priture", "gu"    		    	
            };	
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		if (rs.next()){
    			for(String key:keys){
    				jres.put(key, rs.getString(key));
    			}
    			String comment = rs.getString("comments");
    			comment = comment.replaceAll(DBManager.enterDelimenter,"\n");
    			jres.put("comments", comment);
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			jres.put("create_date", sdf.format(date));
    		}else
    			jres =null;
    		DBManager.closeConnection(conn, pstmt);
    
    		return jres;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
    	

	}
    
	public JSONArray getOffsetPost(int limit,int offset) {
		
		conn = DBManager.getConnection();
		String sql = "SELECT p.id as id, p.u_id as u_id,p.title as title, p.category as category, u.user_name as user_name,"
				+"p.comments as comments, p.good as good,p.hits as hits, p.create_date as create_date, p.priture,gu as priture,gu"
				+" FROM citizen_poster as p, users as u Where p.u_id = u.id order by id desc limit ? offset ?";
     	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, limit);
    		pstmt.setInt(2, offset);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","title","category","good","hits","priture","gu","user_name"    		    	
            };	
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		CitizenPosterCommentDAO dao = new CitizenPosterCommentDAO();
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			String comment = rs.getString("comments");
    			comment = comment.replaceAll(DBManager.enterDelimenter,"\n");
    			item.put("comments", comment);
    			
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			item.put("create_date", sdf.format(date));
    			int count = dao.getCountPosterComment(rs.getInt("id"));
    			item.put("count", count);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}

	public boolean updateHitsPoster(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_poster SET hits = hits+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean addGoodPoster(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_poster SET good = good+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean minusGoodPoster(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_poster SET good = good-1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
    public static void main(String[] argv){
    	CitizenPosterDAO dao = new CitizenPosterDAO();
//    	System.out.println(dao.getCitizenPost(8));
    	System.out.println(dao.getOffsetPost(3, 5));
//    	String id = dao.registerCitizenPoster("test", "test","test");
//    	
//    	dao.updateCitizenPoster(id, "tttt", "tttt");
//    	CitizenPoster a= dao.searchCitizenPosterByID(id);
//    	System.out.println(a.getId());
    	//dao.deletecpost("cpost00001");
    	
    }

}
