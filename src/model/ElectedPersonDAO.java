package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONObject;

public class ElectedPersonDAO {
	private Connection conn;
    private PreparedStatement pstmt;

    
	public JSONObject getElectedPerson(int id) {
		JSONObject jres = new JSONObject();
    	
		conn = DBManager.getConnection();
    	String sql = "SELECT * FROM elected_person WHERE id = ?";
    	
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"person_name","local","born_day","born_region","education","complete","normal",
    				"continues","part","review","priture","party","interesting","term"
            };

    		
    		//ResultSet rs = pstmt.getGeneratedKeys();
    		if (rs.next()){
    			for(String key:keys){
    				jres.put(key, rs.getString(key));
    			}
    		}else
    			jres =null;
    		DBManager.closeConnection(conn, pstmt);
    
    		return jres;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
    	

	}
	
}
