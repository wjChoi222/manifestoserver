package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

import restful.CommentTime;

public class CitizenSayCommentDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    public int insertSayComment(int u_id,int s_id,String comments,boolean opinion){ 	
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO citizen_say_comment(u_id,s_id,comments,create_date, opinion)" +
    			"VALUES (?,?,?,now(),?)";
    	try {
    		pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    		//pstmt.setString(1, id);
    		pstmt.setInt(1,u_id);
    		pstmt.setInt(2,s_id);
    		pstmt.setString(3, comments);
    		pstmt.setBoolean(4, opinion);

    		pstmt.executeUpdate();

    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);
    		      		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }

    
	public JSONArray getOffsetSayComment(int s_id,int limit,int offset) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT id,u_id,s_id,comments,create_date,opinion FROM citizen_say_comment where s_id=? order by id desc limit ? offset ?";
    		
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,s_id);
    		pstmt.setInt(2, limit);
    		pstmt.setInt(3, offset);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","s_id","comments","opinion"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public JSONArray getAllSayComment(int s_id) {
	
		conn = DBManager.getConnection();
    	String sql = "SELECT s.id as id, u.user_name as u_id, s.s_id as s_id, s.comments as comments, s.create_date as create_date, s.opinion as opinion"+
		" FROM citizen_say_comment as s, users as u"+
    	" where s.s_id=? and s.u_id = u.id order by id desc";
    	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);		
    		pstmt.setInt(1,s_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","s_id","comments","opinion"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public int getCountSayComment(int s_id) {
		conn = DBManager.getConnection();
    	String sql = "SELECT count(*) as c FROM citizen_say_comment where s_id=?";	
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,s_id);
    		ResultSet rs = pstmt.executeQuery();
    		int count = 0;
    		while (rs.next()){
    			count = rs.getInt("c");
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return count;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return 0;
    	}
	}
//	public static void main(String args[]){
//		CitizenSayCommentDAO dao = new CitizenSayCommentDAO();
////		for(int i =1; i<10; i++)
////			for(int j =0; j<15; j++) {
////				boolean opinion = true;
////				if(j%3==0)
////					opinion= false;
////				dao.insertSayComment(1, i, "test testtesttesttesttesttest", opinion);
////			}
//		System.out.println(dao.getAllSayComment(4));
//		//		System.out.println(dao.getCountSayComment(1));
////		System.out.println(dao.getOffsetSayComment(1, 5, 10));
//		
//	}
}
