package model;
import java.sql.*;
import java.util.Properties;

public class DBManager {
	static public String enterDelimenter ="#";
	
	 public static Connection getConnection() {
	        Connection conn = null;

	        String jdbc_driver = "com.mysql.jdbc.Driver";
	        String jdbc_id = "root";
	        String jdbc_pw = "manifesto2017";
	        String jdbc_url = "jdbc:mysql://seoul-app-manifesto.cdbwzjwfetqy.ap-northeast-2.rds.amazonaws.com:3306/manifesto?useUnicode=true&characterEncoding=utf8";

	            try{
	                Class.forName(jdbc_driver);
	                conn = DriverManager.getConnection(jdbc_url,jdbc_id, jdbc_pw);
	        } catch (SQLException se) {
	            se.printStackTrace();
	            return null;
	        } catch (ClassNotFoundException ce) {
	            ce.printStackTrace();
	            return null;
	        }
	        return conn;
	    }

	    public static boolean closeConnection (Connection conn, Statement stmt) {
	        try {
	            conn.close();
	            stmt.close();
	            return true;
	        } catch (SQLException se) {
	            se.printStackTrace();
	            return false;
	        }
	    }
}
