package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONObject;

public class CitizenHelpDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    
    public JSONObject getCitizenHelp(int id) {
		JSONObject jres = new JSONObject();
    	
		conn = DBManager.getConnection();
		String sql = "SELECT id,u_id,title,category,comments,good,hits,create_date,priture FROM citizen_help WHERE id = ?";
    	
		try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","title","category","comments","good","hits","priture"    		    	
            };	
    		//ResultSet rs = pstmt.getGeneratedKeys();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    		if (rs.next()){
    			for(String key:keys){
    				jres.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			jres.put("create_date", sdf.format(date));
    		}else
    			jres =null;
    		DBManager.closeConnection(conn, pstmt);
    
    		return jres;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
    	

	}
    
	public JSONArray getOffsetHelp(int limit,int offset) {
		
		conn = DBManager.getConnection();
		String sql = "SELECT id,u_id,title,category,comments,good,bad,hits,create_date,priture FROM citizen_help order by id desc limit ? offset ?";
     	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, limit);
    		pstmt.setInt(2, offset);
    		ResultSet rs = pstmt.executeQuery();


    		String[] keys = new String[] {
    				"id","u_id","title","category","good","bad","hits","priture"    		    	
            };	
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		CitizenHelpCommentDAO dao = new CitizenHelpCommentDAO();
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			String comment = rs.getString("comments");
    			comment = comment.replaceAll(DBManager.enterDelimenter,"\n");
    			item.put("comments", comment);
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			item.put("create_date", sdf.format(date));
    			int count = dao.getCountHelpComment(rs.getInt("id"));
    			item.put("count", count);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	public boolean updateHitsHelp(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_help SET hits =hits+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}

	public boolean addGoodHelp(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_help SET good = good+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean minusGoodHelp(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_help SET good = good-1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public static void main(String args[]){
		CitizenHelpDAO dao = new CitizenHelpDAO();
	}
}
