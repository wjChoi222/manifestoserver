package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

import restful.CommentTime;

public class CommentOpinionDAO {
	public static final int say =1;
	public static final int help =2;
	public static final int post =3;
	public static final int knowledge=4;
	
	public static final int good = 1;
	public static final int bad = 0;
	
	private Connection conn;
    private PreparedStatement pstmt;
    
	public int insertCommentOpinion(int u_id,int c_id,int category, int opinion){ 	
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO comment_opinion(u_id,c_id,category, opinion)" +
    			"VALUES (?,?,?,?)";
    	
    	try {
    		pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    		//pstmt.setString(1, id);
    		pstmt.setInt(1,u_id);
    		pstmt.setInt(2,c_id);
    		pstmt.setInt(3, category);
    		pstmt.setInt(4, opinion);
    		pstmt.executeUpdate();

    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);
    		      		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }
	
	
	public JSONObject getCommentOpinion(int u_id, int c_id, int category) {
			
		conn = DBManager.getConnection();
    	String sql = "SELECT opinion FROM comment_opinion where u_id=? and c_id=? and category=?";
    		
    	JSONObject result = new JSONObject();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,u_id);
    		pstmt.setInt(2, c_id);
    		pstmt.setInt(3, category);
    		ResultSet rs = pstmt.executeQuery();

			result.put("code", "false");
    		while (rs.next()){
    			result.put("opinion", rs.getInt("opinion"));
    			result.put("code", "success");
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public boolean deleteCommentOpinion(int u_id, int c_id, int category) {
		
		conn = DBManager.getConnection();
    	String sql = "DELETE FROM comment_opinion where u_id=? and c_id=? and category=?";
    		
    	JSONObject result = new JSONObject();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, u_id);
    		pstmt.setInt(2, c_id);
    		pstmt.setInt(3, category);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
		
	}
	
	public boolean updateCommentOpinion(int u_id, int c_id, int category,int opinion){
		conn = DBManager.getConnection();
		String sql = "UPDATE comment_opinion SET opinion=? WHERE u_id=? and c_id=? and category=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, opinion);
    		pstmt.setInt(2, u_id);
    		pstmt.setInt(3, c_id);
    		pstmt.setInt(4, category);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
//	public static void main(String args[]){
//		CommentOpinionDAO dao = new CommentOpinionDAO();
////		dao.insertCommentOpinion(1, 1, say, 1);
//		dao.updateCommentOpinion(1, 1, say, 0);
////		System.out.println(dao.deleteCommentOpinion(1, 1, say));
//	}
}
