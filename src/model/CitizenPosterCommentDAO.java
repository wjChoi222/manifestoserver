package model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

import restful.CommentTime;

public class CitizenPosterCommentDAO {

	private Connection conn;
    private PreparedStatement pstmt;
    
    //Create
    public int insertCitizenPosterComment(int u_id, int p_id, String comments) {
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO citizen_poster_comment(u_id, p_id, comments,create_date)" +
    			"VALUES (?,?,?,now())";
    	try {
    		pstmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
    		pstmt.setInt(1,u_id);
    		pstmt.setInt(2,p_id);
    		pstmt.setString(3, comments);

    		pstmt.executeUpdate();
    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);      
    		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }
    
//    //Retrieve
//    public CitizenPosterComment searchCitizenPosterCommentByID(String id) {
//        conn = DBManager.getConnection();
//        String sql = "SELECT * FROM citizen_poster_comment WHERE id=?";
//        CitizenPosterComment result = new CitizenPosterComment();
//
//        try {
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setString(1, id);
//
//            ResultSet rs = pstmt.executeQuery();
//            if (rs.next()) {
//            	result.setId(rs.getString("id"));
//            	result.setU_id(rs.getString("u_id"));
//            	result.setP_Id(rs.getString("p_id"));
//            	result.setComments(rs.getString("comments"));
//            	
//            	DBManager.closeConnection(conn, pstmt);
//                return result;
//            } else {
//                return null;
//            }
//        } catch (SQLException se) {
//            se.printStackTrace();
//            DBManager.closeConnection(conn, pstmt);
//            return null;
//        }
//    }

    
    
    //Update
    public boolean updateCitizenPosterComment(String id, String comments) {
        conn = DBManager.getConnection();
        String sql = "UPDATE citizen_poster_comment SET comments = ? where id =?";

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, comments);
            pstmt.setString(2, id);
            pstmt.executeUpdate();
            DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
    }
    
    //delete
    public boolean deleteCitizenPosterComment(String ID) {
        conn = DBManager.getConnection();
        String sql = "DELETE FROM citizen_poster_comment WHERE id = ?";

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, ID);
            pstmt.executeUpdate();
            DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
    }
    
	public JSONArray getOffsetPosterComment(int p_id,int limit,int offset) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT id,u_id,p_id,comments,create_date FROM citizen_poster_comment where p_id=? order by id desc limit ? offset ?";
    		
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,p_id);
    		pstmt.setInt(2, limit);
    		pstmt.setInt(3, offset);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","p_id","comments"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}

	public JSONArray getAllPosterComment(int p_id) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT p.id as id, u.user_name as u_id, p.p_id as p_id,p.comments as comments,p.create_date as create_date"
    			+" FROM citizen_poster_comment as p,users as u"
    			+" where p.p_id=? and u.id=p.u_id order by id desc";
    	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,p_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","p_id","comments"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public int getCountPosterComment(int p_id) {
		conn = DBManager.getConnection();
    	String sql = "SELECT count(*) as c FROM citizen_poster_comment where p_id=?";	
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,p_id);
    		ResultSet rs = pstmt.executeQuery();
    		int count = 0;
    		while (rs.next()){
    			count = rs.getInt("c");
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return count;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return 0;
    	}
	}
    
    public static void main(String[] argv){
    	CitizenPosterCommentDAO dao = new CitizenPosterCommentDAO();
//    	for(int i =1; i<=10; i++)
//    		for(int j =0;j<=15; j++)
//    			dao.insertCitizenPosterComment(1, i, "test�� ���,test�� ���,test�� ���,test�� ���,test�� ���,test�� ���,test�� ���,");

    	System.out.println(dao.getAllPosterComment(5));
//    	System.out.println(dao.getOffsetPosterComment(1, 2, 2));
    	//    	String id = dao.registerCitizenPosterComment("users00001","cpost00001", "test");
//    	
//    	dao.updateCitizenPosterComment(id, "tttt");
//    	CitizenPosterComment a= dao.searchCitizenPosterCommentByID(id);
//    	System.out.println(a.getId());
//    	//dao.deletecpost("cpost00001");
    	
    }	

    
}
