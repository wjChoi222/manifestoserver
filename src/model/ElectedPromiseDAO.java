package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;

public class ElectedPromiseDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
	public JSONArray getElectedPromise(int ep_id, String category) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT category,contents,state FROM elected_promise WHERE ep_id = ? and category =?";
    	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, ep_id);
    		pstmt.setString(2, category);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"category","contents","state"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}

	public JSONObject getElectedPromiseCountCategory(int ep_id){
		
		conn = DBManager.getConnection();
    	String sql = "SELECT count(if(category='adm', category, null)) as admi,"+
    			"count(if(category='envi', category, null)) as envi,"+
    			"count(if(category='cul', category, null)) as culture,"+
    			"count(if(category='city', category, null)) as city,"+
    			"count(if(category='eco', category, null)) as economy,"+
    			"count(if(category='wel', category, null)) as welfare "+
    			"FROM elected_promise where ep_id=?";
    	
    	JSONObject result = new JSONObject();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,ep_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"admi","envi","culture","city","economy","welfare"
            };
    		
    		if (rs.next()){
    			for(String key:keys){
    				result.put(key, rs.getString(key));
    			}
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}

	public JSONObject getElectedPromiseCountState(int ep_id,String category){
		
		conn = DBManager.getConnection();
    	String sql = "SELECT count(if(state='complete', category, null)) as complete,"+
    			"count(if(state='continues', state, null)) as continues,"+
    			"count(if(state='part', state, null)) as part,"+
    			"count(if(state='review', state, null)) as review,"+
    			"count(if(state='normal', state, null)) as normal "+
    			"FROM elected_promise where ep_id=? and category=? ";
    	JSONObject result = new JSONObject();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,ep_id);
    		pstmt.setString(2, category);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"complete","continues","part","review","normal"
            };
    		
    		if (rs.next()){
    			for(String key:keys){
    				result.put(key, rs.getString(key));
    			}
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}

	public JSONObject getElectedPromiseCountStateAll(int ep_id){
		
		conn = DBManager.getConnection();
    	String sql = "SELECT count(if(state='complete', category, null)) as complete,"+
    			"count(if(state='continues', state, null)) as continues,"+
    			"count(if(state='part', state, null)) as part,"+
    			"count(if(state='review', state, null)) as review,"+
    			"count(if(state='nomal', state, null)) as nomal "+
    			"FROM elected_promise where ep_id=?";
    	JSONObject result = new JSONObject();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,ep_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"complete","continues","part","review","nomal"
            };
    		
    		if (rs.next()){
    			for(String key:keys){
    				result.put(key, rs.getString(key));
    			}
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		ElectedPromiseDAO dao = new ElectedPromiseDAO();
////		JSONArray ja = dao.getElectedPromise(1,"寃쎌젣");
////		System.out.println(ja.toString());
//		//System.out.println(dao.getElectedPromiseCountCategory(1).toString());
//		//System.out.println(dao.getElectedPromiseCountState(1, "蹂듭�"));
//	}

}
