package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import restful.CommentTime;

public class KnowledgeDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    public JSONObject getKnowledge(int id) {
		JSONObject jres = new JSONObject();
    	
		conn = DBManager.getConnection();
		String sql = "SELECT id,title,contents,good,hits,create_date,priture FROM knowledge WHERE id = ?";
    	
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","title","contents","good","hits","create_date","priture"
            };	
    		//ResultSet rs = pstmt.getGeneratedKeys();
    		if (rs.next()){
    			for(String key:keys){
    				jres.put(key, rs.getString(key));
    			}
    		}else
    			jres =null;
    		DBManager.closeConnection(conn, pstmt);
    
    		return jres;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
    	

	}
    
	public JSONArray getOffsetKnowledge(int limit,int offset) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT id,title,contents,good,hits,create_date,priture FROM knowledge order by id desc limit ? offset ?";
    	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		pstmt.setInt(1, limit);    		
    		pstmt.setInt(2, offset);
    		ResultSet rs = pstmt.executeQuery();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		String[] keys = new String[] {
    				"id","title","good","hits","priture"
            };
    		KnowledgeCommentDAO kdao = new KnowledgeCommentDAO();
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			String comment = rs.getString("contents");
    			comment = comment.replaceAll(DBManager.enterDelimenter,"\n");
    			item.put("contents", comment);
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			item.put("create_date", sdf.format(date));
    			int num = kdao.getNumberOfKnowledgeComment(Integer.parseInt(rs.getString("id")));
    			item.put("comment_num", num);
    			
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public boolean addGoodKnowledge(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE knowledge SET good = good+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean minusGoodKnowledge(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE knowledge SET good = good-1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
}
