package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

import restful.CommentTime;

public class CitizenHelpCommentDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    public int insertHelpComment(int u_id,int h_id,String comments){ 	
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO citizen_help_comment(u_id,h_id,comments,create_date)" +
    			"VALUES (?,?,?,now())";
    	try {
    		pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    		//pstmt.setString(1, id);
    		pstmt.setInt(1,u_id);
    		pstmt.setInt(2,h_id);
    		pstmt.setString(3, comments);

    		pstmt.executeUpdate();

    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);
    		      		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }

    
	public JSONArray getOffsetHelpComment(int h_id,int limit,int offset) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT id,u_id,h_id,comments,create_date FROM citizen_help_comment where h_id=? order by id desc limit ? offset ?";
    		
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,h_id);
    		pstmt.setInt(2, limit);
    		pstmt.setInt(3, offset);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","h_id","comments"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public JSONArray getAllHelpComment(int h_id) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT h.id as id, u.user_name as u_id ,h.h_id as h_id,h.comments as comments, h.create_date as create_date"+
		" FROM citizen_help_comment as h, users as u"+
		" where h.h_id=? and u.id = h.u_id order by id desc";
    	JSONArray result = new JSONArray();
    	try {
    		//get
    		pstmt = conn.prepareStatement(sql);
    		pstmt.setInt(1,h_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","h_id","comments"
            };
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public int getCountHelpComment(int h_id) {
		conn = DBManager.getConnection();
    	String sql = "SELECT count(*) as c FROM citizen_help_comment where h_id=?";	
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,h_id);
    		ResultSet rs = pstmt.executeQuery();
    		int count = 0;
    		while (rs.next()){
    			count = rs.getInt("c");
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return count;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return 0;
    	}
	}
	public static void main(String args[]){
		CitizenHelpCommentDAO dao = new CitizenHelpCommentDAO();
		System.out.println(dao.getAllHelpComment(10));
		//		String comments = "test�� ���,test�� ���,test�� ���,test�� ���,test�� ���,";
//		for(int i =1; i<=10; i++)
//			for(int j =0; j<10; j++)
//				dao.insertHelpComment(1, i, comments);
	}
}
