package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

import restful.CommentTime;

public class KnowledgeCommentDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    public int insertKnowledgeComment(int u_id,int k_id,String comments){ 	
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO knowledge_comment(u_id,k_id,comments,create_date)" +
    			"VALUES (?,?,?,now())";
    	
    	try {
    		pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    		
    		//pstmt.setString(1, id);
    		pstmt.setInt(1,u_id);
    		pstmt.setInt(2,k_id);
    		pstmt.setString(3, comments);

    		pstmt.executeUpdate();

    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);
    		      		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }

    
	public JSONArray getOffsetKnowledgeComment(int k_id,int limit,int offset) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT id,u_id,k_id,comments,create_date FROM knowledge_comment where k_id = ? order by id desc limit ? offset ?";
    		
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,k_id);
    		pstmt.setInt(2, limit);
    		pstmt.setInt(3, offset);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","k_id","comments",
            };
    		
    		
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	
	public JSONArray getAllKnowledgeComment(int k_id) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT k.id as id, u.user_name as u_id, k.k_id as k_id, k.comments as comments, k.create_date as create_date "+
		"FROM knowledge_comment as k, users as u where k.k_id = ? and k.u_id = u.id order by id desc";
    		
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,k_id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","k_id","comments",
            };
    		
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			String time = CommentTime.getDiffTime(date);
    			item.put("create_date", time);
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	
	public int getNumberOfKnowledgeComment(int k_id) {
		
		conn = DBManager.getConnection();
    	String sql = "SELECT count(*) as num FROM knowledge_comment where k_id = ?";
    	int num = 0;
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1,k_id);

    		ResultSet rs = pstmt.executeQuery();

    		while (rs.next()){
    			num= rs.getInt("num");    			
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return num;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return 0;
    	}
		
	}
	
	
//	public static void main(String args[]){
//		KnowledgeCommentDAO dao = new KnowledgeCommentDAO();
//		JSONArray ja = null;
//		for(int i =0; i<9;i++){
//			ja = dao.getTenKnowledgeComment(1,i);
//			System.out.println(ja.toString());
//		}
//	}
}
