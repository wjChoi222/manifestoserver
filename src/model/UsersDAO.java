package model;
import java.sql.*;

import org.json.JSONObject;
public class UsersDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    //Create
    public int registerUsers(String email, String pw, String user_name) {
    	conn = DBManager.getConnection();
    	String sql = "INSERT INTO users(email, pw, user_name,create_date)" +
    			"VALUES (?,?,?,now())";
    	//String id =generateID();
    	try {
    		pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    		
    		//pstmt.setString(1, id);
    		pstmt.setString(1,email);
    		pstmt.setString(2, pw);
    		pstmt.setString(3, user_name);

    		pstmt.executeUpdate();

    		ResultSet rs = pstmt.getGeneratedKeys();
    		int id =-1;
    		if (rs.next())
    		  id = rs.getInt(1);
    		      		
    		DBManager.closeConnection(conn, pstmt);
    		return id;
    	} catch (SQLException se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return -1;
    	}
    }
    
    //Retrieve
    public JSONObject searchUserByEmail(String email) {
        conn = DBManager.getConnection();
        String sql = "SELECT id,email,pw,user_name,create_date FROM users WHERE email=?";
       
        
        
        JSONObject result = new JSONObject();
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, email);

            ResultSet rs = pstmt.executeQuery();
            
    		String[] keys = new String[] {
    				"id","email","pw","user_name","create_date"
            };	
    		//ResultSet rs = pstmt.getGeneratedKeys();
    		if (rs.next()){
    			for(String key:keys){
    				result.put(key, rs.getString(key));
    			}
    		}else{
    			result =null;
    		}
    		DBManager.closeConnection(conn, pstmt);
            return result;
        } catch (Exception se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return null;
        }
    }

    
    
    //Update
    public boolean updateUser(int id, String pw, String name) {
        conn = DBManager.getConnection();
        String sql = "UPDATE users SET pw=?, user_name = ? where id =?";

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, pw);
            pstmt.setString(2, name);
            pstmt.setInt(3, id);
            pstmt.executeUpdate();
            DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
    }
    
    
    //delete
    public boolean deleteUsers(String ID) {
        conn = DBManager.getConnection();
        String sql = "DELETE FROM users WHERE id = ?";

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, ID);
            pstmt.executeUpdate();
            DBManager.closeConnection(conn, pstmt);
            return true;
        } catch (SQLException se) {
            se.printStackTrace();
            DBManager.closeConnection(conn, pstmt);
            return false;
        }
    }
    
//    
//    public static void main(String[] argv){
//    	UsersDAO dao = new UsersDAO();
//    	int id = dao.registerUsers("222","test", "test");
//    	
//    	dao.updateUser(id, "tttt", "tttt");
//    	Users a= dao.searchUserByEmail("test");
//    	System.out.println(a.getUser_name());
//    	dao.deleteUsers("users00001");
//    	ElectedPersonDAO Edao = new ElectedPersonDAO();
//		JSONObject j = Edao.getElectedPerson(1);
//
//		System.out.println(j.toString());
//    }
}
