package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONObject;

public class CitizenSayDAO {
	private Connection conn;
    private PreparedStatement pstmt;
    
    
    public JSONObject getCitizenSay(int id) {
		JSONObject jres = new JSONObject();
    	
		conn = DBManager.getConnection();
		String sql = "SELECT id,u_id,title,category,comments,good,bad,hits,create_date,priture,agree,opposite FROM citizen_say WHERE id = ?";
    	
		try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, id);
    		ResultSet rs = pstmt.executeQuery();

    		String[] keys = new String[] {
    				"id","u_id","title","category","comments","good","bad","hits","create_date","priture","agree","opposite"		    	
            };	
    		//ResultSet rs = pstmt.getGeneratedKeys();
    		if (rs.next()){
    			for(String key:keys){
    				jres.put(key, rs.getString(key));
    			}
    		}else
    			jres =null;
    		DBManager.closeConnection(conn, pstmt);
    
    		return jres;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
    	

	}
    
	public JSONArray getOffsetSay(int limit,int offset) {
		
		conn = DBManager.getConnection();
		String sql = "SELECT id,u_id,title,category,comments,good,bad,hits,create_date,priture,agree,opposite FROM citizen_say order by id desc limit ? offset ?";
     	
    	JSONArray result = new JSONArray();
    	try {
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, limit);
    		pstmt.setInt(2, offset);
    		ResultSet rs = pstmt.executeQuery();


    		String[] keys = new String[] {
    				"id","u_id","title","category","good","bad","hits","priture","agree","opposite"    		    	
            };	
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		CitizenSayCommentDAO dao = new CitizenSayCommentDAO();
    		
    		while (rs.next()){
    			JSONObject item = new JSONObject();
    			for(String key:keys){
    				item.put(key, rs.getString(key));
    			}
    			String comment = rs.getString("comments");
    			comment = comment.replaceAll(DBManager.enterDelimenter,"\n");
    			item.put("comments", comment);
    			java.util.Date date = new java.util.Date(rs.getTimestamp("create_date").getTime());
    			item.put("create_date", sdf.format(date));
    			int count = dao.getCountSayComment(rs.getInt("id"));
    			item.put("count", count);
    			
    			result.put(item);
    		}
    		DBManager.closeConnection(conn, pstmt);
    
    		return result;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return null;
    	}
		
	}
	public boolean updateHitsSay(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_say SET hits = hits+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean addGoodSay(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_say SET good = good+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean minusGoodSay(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_say SET good = good-1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	
	public boolean addBadSay(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_say SET bad = bad+1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	public boolean minusBadSay(int id) {
		conn = DBManager.getConnection();
		String sql = "UPDATE citizen_say SET bad = bad-1 where id=?";
    	try {
    		pstmt = conn.prepareStatement(sql);	
    		pstmt.setInt(1, id);
    		pstmt.executeUpdate();
    		DBManager.closeConnection(conn, pstmt);
    		return true;
    	} catch (Exception se) {
    		se.printStackTrace();
    		DBManager.closeConnection(conn, pstmt);
    		return false;
    	}
	}
	
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		CitizenSayDAO dao = new CitizenSayDAO();
//		dao.temp();
////		System.out.println(dao.getOffsetSay(2, 0).toString());	
////		System.out.println(dao.getOffsetSay(4, 4).toString());
////		System.out.println(dao.getCitizenSay(4));
//	}

}
