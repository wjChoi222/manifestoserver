package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.KnowledgeDAO;
import model.UsersDAO;

public class UserInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();

		String email = request.getParameter("email");
		String nickname = request.getParameter("nickname");
		try{
			if(email == null || nickname ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				int id = 0;
				UsersDAO dao = new UsersDAO();
				JSONObject jres = dao.searchUserByEmail(email);
				if(jres == null) {
					id = dao.registerUsers(email,"empty",nickname);
				}
				else {
					id = jres.getInt("id");
				}
				jsonRes.put("id", id);
				jsonRes.put("code", "SUCCESS");
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
