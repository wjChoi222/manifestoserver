package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.ElectedPersonDAO;
import model.ElectedProfileDAO;
import model.ElectedPromiseDAO;

/**
 * Servlet implementation class ElectedPersonPromiseServlet
 */
@WebServlet("/ElectedPersonPromise")
public class ElectedPersonPromiseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();
		response.setContentType("text/html;charset=UTF-8"); 
		String id =  request.getParameter("ep_id");
		String category = request.getParameter("category");
		try{
			if(id == null || category ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				System.out.println(category);
				int ep_id =Integer.parseInt(id);
				ElectedPromiseDAO dao =  new ElectedPromiseDAO();
				JSONArray promise = dao.getElectedPromise(ep_id, category);
				JSONObject count = dao.getElectedPromiseCountState(ep_id, category);
				jsonRes.put("promise", promise);
				jsonRes.put("count", count);
				jsonRes.put("code", "SUCCESS");
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
