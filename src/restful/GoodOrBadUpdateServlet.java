package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import model.CitizenHelpDAO;
import model.CitizenPosterDAO;
import model.CitizenSayDAO;
import model.CommentOpinionDAO;
import model.KnowledgeDAO;
import model.UsersDAO;

public class GoodOrBadUpdateServlet  extends HttpServlet{
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();
		String u_id_str = request.getParameter("u_id");
		String c_id_str = request.getParameter("c_id");
		String category =request.getParameter("category");
		String option = request.getParameter("option");
		try{
			if(category == null || c_id_str ==null || u_id_str == null || option ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				CommentOpinionDAO dao = new CommentOpinionDAO();
				if(category.compareTo("say")==0) {
					CitizenSayDAO dao2 = new CitizenSayDAO();
					// 1 agree  0 opposite
					String opinion = request.getParameter("opinion");
					if(option.compareTo("delete")==0) {
						dao.deleteCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.say);
						if(Integer.parseInt(opinion)==1) {
							dao2.minusGoodSay(Integer.parseInt(c_id_str));
						}else {
							dao2.minusBadSay(Integer.parseInt(c_id_str));
						}
					}else {
						String exist = request.getParameter("exist");
						if(opinion != null) {
							if(Integer.parseInt(exist)==0) {
								dao.insertCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.say,Integer.parseInt(opinion));				
							}else {
								dao.updateCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.say,Integer.parseInt(opinion));
								if(Integer.parseInt(opinion)==1) {
									dao2.minusBadSay(Integer.parseInt(c_id_str));
								}else {
									dao2.minusGoodSay(Integer.parseInt(c_id_str));
								}
							}
							if(Integer.parseInt(opinion)==1) {
								dao2.addGoodSay(Integer.parseInt(c_id_str));
							}else {
								dao2.addBadSay(Integer.parseInt(c_id_str));
							}
						}						
					}
					jsonRes.put("code", "SUCCESS");
				}else if(category.compareTo("help")==0) {
					CitizenHelpDAO dao2 = new CitizenHelpDAO();
					if(option.compareTo("delete")==0) {
						dao.deleteCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.help);
						dao2.minusGoodHelp(Integer.parseInt(c_id_str));
					}else {
						dao.insertCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.help,CommentOpinionDAO.good);						
						dao2.addGoodHelp(Integer.parseInt(c_id_str));
					}
					jsonRes.put("code", "SUCCESS");
				}else if(category.compareTo("post")==0) {
					CitizenPosterDAO dao2 = new CitizenPosterDAO();
					if(option.compareTo("delete")==0) {
						dao.deleteCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.post);
						dao2.minusGoodPoster(Integer.parseInt(c_id_str));
					}else {
						dao.insertCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.post,CommentOpinionDAO.good);						
						dao2.addGoodPoster(Integer.parseInt(c_id_str));
					}
					jsonRes.put("code", "SUCCESS");	
				}else if(category.compareTo("know")==0) {
					KnowledgeDAO dao2 = new KnowledgeDAO();
					if(option.compareTo("delete")==0) {
						dao.deleteCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.knowledge);
						dao2.minusGoodKnowledge(Integer.parseInt(c_id_str));
					}else {
						dao.insertCommentOpinion(Integer.parseInt(u_id_str), Integer.parseInt(c_id_str), CommentOpinionDAO.knowledge,CommentOpinionDAO.good);						
						dao2.addGoodKnowledge(Integer.parseInt(c_id_str));
					}
				}
				else {
					jsonRes.put("code", "FAILURE");						
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
