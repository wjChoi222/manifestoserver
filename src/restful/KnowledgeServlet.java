package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.ElectedPromiseDAO;
import model.KnowledgeDAO;

/**
 * Servlet implementation class KnowledgeServlet
 */
@WebServlet("/Knowledge")
public class KnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();

		String offset = request.getParameter("offset");
		String limit = request.getParameter("limit");
		try{
			if(offset == null || limit ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				int lim =Integer.parseInt(limit);
				int off = Integer.parseInt(offset);
				jsonRes.put("code", "SUCCESS");
				JSONArray list = (new KnowledgeDAO()).getOffsetKnowledge(lim, off);
				jsonRes.put("list", list);
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
