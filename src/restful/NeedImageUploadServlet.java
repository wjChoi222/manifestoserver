package restful;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import model.UsersDAO;

public class NeedImageUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();

	
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		  
		  
		  String savePath = "need";
		  int uploadFileSizeLimit = 1024 * 1024;
		  String encType = "UTF-8";
		  
		  ServletContext context = getServletContext();
		  String uploadFilePath = context.getRealPath(savePath);
		  
		  try {
			  jsonRes.put("code","failure");
			  
			   MultipartRequest multi = new MultipartRequest(request, uploadFilePath,uploadFileSizeLimit,encType,
			                                                                     new DefaultFileRenamePolicy());
			   jsonRes.put("uploadFilePath",uploadFilePath);
			   Enumeration files = multi.getFileNames();
			   while (files.hasMoreElements()) {
				    String file = (String)files.nextElement();
				    String file_name = multi.getFilesystemName(file);
				    String ori_file_name = multi.getOriginalFileName(file);
				    
				    jsonRes.put("file_name",file_name);
				    jsonRes.put("ori_file_name",ori_file_name);
				    
			   	}
			   	jsonRes.put("code","success");
		  	} catch (Exception e) {
		  		e.printStackTrace();
		  	}	
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
