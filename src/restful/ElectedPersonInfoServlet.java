package restful;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.DBManager;
import model.ElectedPersonDAO;
import model.ElectedProfileDAO;
import model.ElectedPromiseDAO;

/**
 * Servlet implementation class ElectedPersonInfo
 */
@WebServlet("/ElectedPersonInfo")
public class ElectedPersonInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		// TODO Auto-generated method stub
		JSONObject jsonRes = new JSONObject();
		String id =  request.getParameter("ep_id");
		try{
			if(id == null){
				System.out.println("id is null");
				jsonRes.put("code", "FAILURE");
			}else{
				int ep_id =Integer.parseInt(id);
				JSONObject elected = (new ElectedPersonDAO()).getElectedPerson(ep_id);
				jsonRes.put("person", elected);
				JSONArray profile =(new ElectedProfileDAO()).getElectedProfile(ep_id);
				jsonRes.put("profile",profile);
				JSONObject num = (new ElectedPromiseDAO()).getElectedPromiseCountCategory(ep_id);
				jsonRes.put("promise_num",num);
				jsonRes.put("code", "SUCCESS");
				
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
        }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
