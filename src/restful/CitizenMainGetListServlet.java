package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.CitizenHelpDAO;
import model.CitizenPosterDAO;
import model.CitizenSayDAO;

/**
 * Servlet implementation class CitizenMainGetList
 */
@WebServlet("/CitizenMainGetList")
public class CitizenMainGetListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();

		try{
			int limit = 3;
			int offset = 0;
			CitizenHelpDAO dao = new CitizenHelpDAO();
			JSONArray list =  dao.getOffsetHelp(limit,offset);
			jsonRes.put("help", list);

			CitizenSayDAO daoSay = new CitizenSayDAO();
			JSONArray say =  daoSay.getOffsetSay(limit,offset);
			jsonRes.put("say", say);

			CitizenPosterDAO daoPost = new CitizenPosterDAO();
			JSONArray post =  daoPost.getOffsetPost(limit,offset);
			jsonRes.put("post", post);

			jsonRes.put("code", "SUCCESS");					

		}catch (Exception e) {
			e.printStackTrace();
		}
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
