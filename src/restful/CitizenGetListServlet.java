package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.CitizenHelpDAO;
import model.CitizenPosterDAO;
import model.CitizenSayDAO;
import model.ElectedPersonDAO;
import model.ElectedProfileDAO;
import model.ElectedPromiseDAO;

/**
 * Servlet implementation class CitizenGetList
 */
@WebServlet("/CitizenGetList")
public class CitizenGetListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();
		String category =  request.getParameter("category");
		String offset_str = request.getParameter("offset");
		int limit = 10;
		try{
			if(category == null || offset_str ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				int offset =Integer.parseInt(offset_str);
				if(category.compareTo("help")==0) {
					CitizenHelpDAO dao = new CitizenHelpDAO();
					JSONArray list =  dao.getOffsetHelp(limit,offset);
					jsonRes.put("list", list);
					
					jsonRes.put("code", "SUCCESS");
				}else if(category.compareTo("say") ==0) {
					CitizenSayDAO dao = new CitizenSayDAO();
					JSONArray list =  dao.getOffsetSay(limit,offset);
					jsonRes.put("list", list);
					
					jsonRes.put("code", "SUCCESS");
				}else if(category.compareTo("post")==0) {
					CitizenPosterDAO dao = new CitizenPosterDAO();
					JSONArray list =  dao.getOffsetPost(limit,offset);
					jsonRes.put("list", list);
					
					jsonRes.put("code", "SUCCESS");					
				}
				else {
					jsonRes.put("code", "FAILURE");
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
