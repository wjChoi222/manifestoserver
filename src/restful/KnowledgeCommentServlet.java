package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.CommentOpinionDAO;
import model.KnowledgeCommentDAO;
import model.KnowledgeDAO;

/**
 * Servlet implementation class KnowledgeCommentDAO
 */
@WebServlet("/KnowledgeComment")
public class KnowledgeCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();

		String k_id_str = request.getParameter("k_id");
		String u_id_str = request.getParameter("u_id");
		try{
			if(k_id_str ==null|| u_id_str ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				int k_id = Integer.parseInt(k_id_str);
				int u_id = Integer.parseInt(u_id_str);
				
				jsonRes.put("code", "SUCCESS");
				
				CommentOpinionDAO dao2 = new CommentOpinionDAO();
				JSONObject opinion = dao2.getCommentOpinion(u_id, k_id, CommentOpinionDAO.knowledge);
				jsonRes.put("opinion",opinion);
				
				JSONArray list = (new KnowledgeCommentDAO()).getAllKnowledgeComment(k_id);
				jsonRes.put("list", list);
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
