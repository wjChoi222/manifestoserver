package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import model.CitizenHelpCommentDAO;
import model.CitizenPosterCommentDAO;
import model.CitizenPosterDAO;
import model.CitizenSayCommentDAO;

/**
 * Servlet implementation class CitizenPosterInsert
 */
@WebServlet("/CitizenPosterInsert")
public class CitizenPosterInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
JSONObject jsonRes = new JSONObject();
		
		
		String u_id_str = request.getParameter("u_id");
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String comments = request.getParameter("comments");
		String gu = request.getParameter("gu");
		String priture = request.getParameter("priture");
		try{
			if(category == null || comments == null || u_id_str ==null || title ==null || gu==null){
				jsonRes.put("code", "FAILURE");
			}else{
				int u_id = Integer.parseInt(u_id_str);
				CitizenPosterDAO dao = new CitizenPosterDAO();
				int result = dao.registerCitizenPoster(u_id, title, category, comments,gu,priture);
				if(result >-1) {
					jsonRes.put("id", result);
					jsonRes.put("code", "SUCCESS");
				}else {
					jsonRes.put("code", "FAILURE");
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
