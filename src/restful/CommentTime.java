package restful;
import java.text.SimpleDateFormat;

public class CommentTime {
	final static long YEAR = 365l;
	final static long HOUR = 24l;
	final static long MINUTE = 60l;
	final static long SECOND = 60l;
	final static long MILISECOND = 1000l;
			
	public static String getDiffTime(java.util.Date date){
		long pastTime = date.getTime();
		java.util.Date currentDate = new java.util.Date();
		long currentTime = System.currentTimeMillis();
		long diff = currentTime-pastTime;
		
		long year = diff/(YEAR*HOUR*MINUTE*SECOND*MILISECOND);
		if(year>0)
			return year+"년 전";
		
		long day = diff/(HOUR*MINUTE*SECOND*MILISECOND);
		diff = diff%(HOUR*MINUTE*SECOND*MILISECOND);
		if(day>0)
			return day+"일 전";
		
		long hour = diff/(MINUTE*SECOND*MILISECOND);
		diff = diff%(MINUTE*SECOND*MILISECOND);
		if(hour>0)
			return hour+"시간 전";
		
		long minute = diff/(SECOND*MILISECOND);
		if(minute>0)
			return minute+"분 전";
		else
			return "방금";
	}
}
