package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.CitizenHelpCommentDAO;
import model.CitizenPosterCommentDAO;
import model.CitizenSayCommentDAO;
import model.KnowledgeCommentDAO;

/**
 * Servlet implementation class CitizenCommentInsertServlet
 */
@WebServlet("/CitizenCommentInsert")
public class CitizenCommentInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();
		String category = request.getParameter("category");
		String u_id_str = request.getParameter("u_id");
		String h_id_str = request.getParameter("h_id");
		String comments= request.getParameter("comment");
		try{
			if(category == null || comments == null || u_id_str ==null || h_id_str ==null){
				jsonRes.put("code", "FAILURE");
			}else{
				int u_id = Integer.parseInt(u_id_str);
				int h_id =Integer.parseInt(h_id_str);
				int result = -1;
				if(category.compareTo("help")==0) {
					CitizenHelpCommentDAO dao = new CitizenHelpCommentDAO();
					result =  dao.insertHelpComment(u_id, h_id, comments);
				}else if(category.compareTo("say") ==0) {
					CitizenSayCommentDAO dao = new CitizenSayCommentDAO();
					String opinion = request.getParameter("opinion");
					result = dao.insertSayComment(u_id, h_id, comments,Boolean.parseBoolean(opinion));
				}else if(category.compareTo("post")==0) {
					CitizenPosterCommentDAO dao = new CitizenPosterCommentDAO();
					result = dao.insertCitizenPosterComment(u_id, h_id, comments);			
				}else if( category.compareTo("know")==0) {
					KnowledgeCommentDAO dao = new KnowledgeCommentDAO();
					result = dao.insertKnowledgeComment(u_id, h_id, comments);
				}
				
				if( result>-1) {
					jsonRes.put("code", "SUCCESS");
					jsonRes.put("id", result);
						
				}else {
					jsonRes.put("code", "FAILURE");
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
