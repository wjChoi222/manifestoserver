package restful;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.CitizenHelpCommentDAO;
import model.CitizenHelpDAO;
import model.CitizenPosterCommentDAO;
import model.CitizenPosterDAO;
import model.CitizenSayCommentDAO;
import model.CitizenSayDAO;
import model.CommentOpinionDAO;

/**
 * Servlet implementation class CitizenGetCommentList
 */
@WebServlet("/CitizenGetCommentList")
public class CitizenGetCommentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonRes = new JSONObject();
		String id_str = request.getParameter("id");
		String category =  request.getParameter("category");
		String offset_str = request.getParameter("offset");
		String u_id_str = request.getParameter("u_id");
		int limit = 10;
		try{
			if(category == null || offset_str ==null ||u_id_str == null){
				jsonRes.put("code", "FAILURE");
			}else{
				int id = Integer.parseInt(id_str);
				int offset =Integer.parseInt(offset_str);
				int u_id = Integer.parseInt(u_id_str);
				CommentOpinionDAO dao2 = new CommentOpinionDAO();
				if(category.compareTo("help")==0) {
					CitizenHelpCommentDAO dao = new CitizenHelpCommentDAO();
					//JSONArray list =  dao.getOffsetHelpComment(id,limit,offset);
					if ((new CitizenHelpDAO()).updateHitsHelp(id)) {
						JSONArray list =  dao.getAllHelpComment(id);
						JSONObject opinion = dao2.getCommentOpinion(u_id, id, CommentOpinionDAO.help);
						jsonRes.put("opinion",opinion);
						jsonRes.put("list", list);
						jsonRes.put("code", "SUCCESS");
					}else {
						jsonRes.put("code", "FAILURE");
					}
				}else if(category.compareTo("say") ==0) {
					CitizenSayCommentDAO dao = new CitizenSayCommentDAO();					
					if ((new CitizenSayDAO()).updateHitsSay(id)) {
						JSONArray list = dao.getAllSayComment(id);
						JSONObject opinion = dao2.getCommentOpinion(u_id, id, CommentOpinionDAO.say);
						jsonRes.put("opinion",opinion);
						jsonRes.put("list", list);
						jsonRes.put("code", "SUCCESS");
					}else {
						jsonRes.put("code", "FAILURE");
					}
				}else if(category.compareTo("post")==0) {
					CitizenPosterCommentDAO dao = new CitizenPosterCommentDAO();
//					JSONArray list =  dao.getOffsetPosterComment(id,limit,offset);

					if ((new CitizenPosterDAO()).updateHitsPoster(id)) {
						JSONArray list = dao.getAllPosterComment(id);
						JSONObject opinion = dao2.getCommentOpinion(u_id, id, CommentOpinionDAO.post);
						jsonRes.put("opinion",opinion);
						jsonRes.put("list", list);
						jsonRes.put("code", "SUCCESS");
					}else {
						jsonRes.put("code", "FAILURE");
					}		
				}
				else {
					jsonRes.put("code", "FAILURE");
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.print(jsonRes);
        out.flush();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
